const initialState = {
  todos: [],
};

const todosReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'addTodo': {
      return { ...state, todos: [action.todo, ...state.todos] };
    }
    case 'removeTodo': {
      return {
        ...state,
        todos: [
          ...state.todos.slice(0, action.index),
          ...state.todos.slice(action.index + 1),
        ],
      };
    }
    default: {
      return state;
    }
  }
};

export { todosReducer, initialState };
