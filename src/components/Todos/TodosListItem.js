import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';

function TodosListItem({ todo, onRemoveTodo }) {
  return (
    <ListItem button>
      <ListItemText primary={todo.date.toDateString()} secondary={todo.todo} />
      <ListItemSecondaryAction>
        <IconButton aria-label="Delete" onClick={onRemoveTodo}>
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
}

export default TodosListItem;
