import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import TodosListItem from './TodosListItem';
import Card from 'components/Card/Card';

const styles = () => ({
  root: {
    height: 500,
  },
  list: {
    maxHeight: 450,
    overflow: 'auto',
  },
});

function TodosList({ classes, todos, onRemoveTodo }) {
  if (!todos.length) {
    return <div className={classes.root} />;
  }
  return (
    <div className={classes.root}>
      <Card>
        <List className={classes.list}>
          {todos.map((todo, index) => (
            <div key={index}>
              {index > 0 && <Divider />}
              <TodosListItem
                todo={todo}
                onRemoveTodo={() => onRemoveTodo(index)}
              />
            </div>
          ))}
        </List>
      </Card>
    </div>
  );
}

TodosList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TodosList);
