import React, { useReducer } from 'react';
import TodosCreate from './TodosCreate';
import TodosList from './TodosList';
import { todosReducer, initialState } from 'reducers/todos';

function Todos() {
  const [{ todos }, dispatch] = useReducer(todosReducer, initialState);
  const onAddTodo = async todo => {
    dispatch({ type: 'addTodo', todo });
  };
  const onRemoveTodo = async index => {
    dispatch({ type: 'removeTodo', index });
  };
  return (
    <>
      <TodosCreate onAddTodo={onAddTodo} />
      <TodosList todos={todos} onRemoveTodo={onRemoveTodo} />
    </>
  );
}

export default Todos;
