import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Card from 'components/Card/Card';

const styles = theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 225,
  },
});

function TodosCreate({ onAddTodo, classes }) {
  const [todo, setTodo] = useState('');
  const onHandleAddToDo = async e => {
    e.preventDefault();
    onAddTodo({
      todo,
      date: new Date(),
    });
    setTodo('');
  };
  return (
    <Card>
      <form onSubmit={onHandleAddToDo}>
        <Grid
          container
          justify="center"
          alignItems="flex-end"
          className={classes.root}
          spacing={16}
        >
          <Grid item>
            <TextField
              id="todo"
              label="What do you need to do today?"
              className={classes.textField}
              value={todo}
              onChange={e => setTodo(e.target.value)}
            />
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              onClick={onHandleAddToDo}
              disabled={!todo.length}
            >
              Add
            </Button>
          </Grid>
        </Grid>
      </form>
    </Card>
  );
}

TodosCreate.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TodosCreate);
