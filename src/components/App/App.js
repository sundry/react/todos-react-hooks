import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Todos from 'components/Todos/Todos';

const styles = () => ({
  root: {
    flexGrow: 1,
    width: '100%',
    height: '100%',
  },
});

function App(props) {
  const { classes } = props;
  return (
    <Grid
      className={classes.root}
      container
      justify="center"
      alignItems="center"
      spacing={16}
    >
      <Grid item>
        <Todos />
      </Grid>
    </Grid>
  );
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
