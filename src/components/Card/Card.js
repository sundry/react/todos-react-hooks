import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    minWidth: 350,
    marginBottom: theme.spacing.unit,
  },
  title: {
    fontSize: 14,
  },
});

function SimpleCard(props) {
  const { classes } = props;
  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography className={classes.title} component="h1">
          {props.title}
        </Typography>
        {props.children}
      </CardContent>
      <CardActions>{props.actions}</CardActions>
    </Card>
  );
}

SimpleCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleCard);
